var statusCode = {
  "SOMETHING_WENT_WRONG":{
    code: 422,
    message: 'Something Went Wrong'
  },
  "DATA_ALREADY_EXISTS":{
    code: 409,
    message: "Data Already Exists"
  },
  "FILE_NOT_UPLOADED": {
    code: 422,
    message: "File not uploaded"
  },
  "SUCCESS": {
    code: 200,
    message: "Success"
  },
  "INVALID_POST_ID": {
    code: 400,
    message: "Invalid post id"
  },
  "INVALID_USER_ID": {
    code: 400,
    message: "Invalid user id"
  },
  "INVALID_ACCESS": {
    code: 400,
    message: "Invalid access"
  },
  "INVALID_MAPPING": {
    code: 400,
    message: "Invalid program mapping"
  },
  "INVALID_SHEET_SETTINGS": {
    code: 400,
    message: "Invalid sheet settings"
  },
  "SHEET_ERROR": {
    code: 400,
    message: "Please validate the sheet and retry"
  },
  "INVALID_TIMEZONE": {
    code: 400,
    message: "Invalid timezone"
  },
  "FILE_NOT_FOUND": {
    code: 400,
    message: "File not found"
  },
  "PAGE_ITEMS_NOT_NUMBER": {
    code: 400,
    message: "Page and items must be number"
  },
  "INVALID_DATE": {
    code: 400,
    message: "Invalid date"
  },
  "AUTHENTICATION_FAILED":{
    code: 401,
    message: 'Authentication Failed'
  },
  "TOKEN_AUTHENTICATION_FAILED":{
    code: 401,
    message: 'TOKEN_AUTHENTICATION_FAILED'
  },
  "INVALID_EMAIL":{
    code: 400,
    message: "Invalid Email"
  },
  "INVALID_PHONE":{
    code: 400,
    message: "Invalid Phone"
  },
  "PHONE_IS_REQUIRED":{
    code: 400,
    message: "Phone number is required"
  },
  "EMAIL_IS_REQUIRED":{
    code: 400,
    message: "Email is required"
  },
  "INVALID_DOB":{
    code: 400,
    message: "Invalid DOB"
  },
  "INVALID_Sex":{
    code: 400,
    message: "Invalid Sex"
  },
  "USER_NOT_FOUND":{
    code: 404,
    message: "User Not Found"
  },
  "GOOGLE_TOKEN_EXPIRED":{
    code: 401,
    message: "Google Token Expired"
  },
  "WRONG_PASSWORD":{
    code: 403,
    message: "Wrong Password"
  },
  "LOGGED_IN_TOO_MANY_PLACES":{
    code: 403,
    message: "Logged In Too Many Places"
  },
  "LIMIT_EXCEEDED":{
    code: 403,
    message: "Limit Exceeded"
  },
  "REFERRAL_CODE_NOT_FOUND":{
    code: 404,
    message: "Referral Code Not Found"
  },
  "INVALID_PASSWORD_SIGNUP_SIZE":{
    code: 400,
    message: "Invalid Password Minimum Length 8"
  },
  "INVALID_COUNTRY_CODE":{
    code: 400,
    message: "Invalid Country Code"
  },
  "INVALID_FIRST_NAME":{
    code: 400,
    message: "Invalid First Name"
  },
  "USER_ALREADY_EXISTS":{
    code: 409,
    message: "User Already Exists"
  },
  "REFERRAL_USER_NOT_FOUND":{
    code: 404,
    message: "Referral user is not found"
  },
  "REFERRAL_CODE_USED_ALREADY":{
    code: 409,
    message: "Referral code has been used already"
  },
  "INVALID_PARAMETERS":{
    code: 400,
    message: "Invalid Parameters"
  },
  "MOBILE_NOT_VERIFIED":{
    code: 403,
    message: "Mobile has not been verified"
  },
  "MOBILE_ALREADY_VERIFIED":{
    code: 403,
    message: "Mobile is already verified"
  },
  "EMAIL_ALREADY_VERIFIED":{
    code: 403,
    message: "Email is already verified"
  },
  "OTP_INCORRECT":{
    code: 403,
    message: "OTP is Incorrect"
  },
  "USER_ALREADY_EXISTS_BY_PHONE": {
    code: 403,
    message: "Phone is already in use"
  },
  "USER_ALREADY_EXISTS_BY_EMAIL": {
    code: 403,
    message: "Email is already in use"
  },
  "PAASSWORD_IS_REQUIRED": {
    code: 400,
    message: "Password is required"
  },
  "CONFIRM_PAASSWORD_IS_REQUIRED": {
    code: 400,
    message: "Confirm password is required"
  },
  "NOT_A_NORMAL_SIGNUP": {
    code: 403,
    message: "Not signed in normal signup , try google , fb instead"
  },
  "OTP_IS_REQUIRED": {
    code: 400,
    message: "OTP is required"
  },
  "EMAIL_OR_PHONE_NUMBER_IS_REQUIRED": {
    code: 400,
    message: "Email or Phone number is required"
  },
  "EMAIL_AND_PHONE_NUMBER_IS_REQUIRED": {
    code: 400,
    message: "Email and Phone number are required"
  },
  "OTP_NOT_FOUND": {
    code: 404,
    message: "OTP not found"
  },
  "WRONG_OTP": {
    code: 422,
    message: "Wrong OTP Entered"
  },
  "BAD_REQUEST": {
    code: 400,
    message: "Bad request"
  },
  "INCORRECT_TOKEN_AND_SECRET": {
    code: 401,
    message: "Incorrect token and token secret"
  },
  "INCORRECT_TOKEN": {
    code: 401,
    message: "Incorrect token"
  },
  "TOKEN_IS_REQUIRED": {
    code: 401,
    message: "Token is required"
  },
  "DEVICENAME_AND_CLIENTAPIKEY_AND_AUTHTOKEN_ARE_REQUIRED": {
    code: 400,
    message: "deviceName, clientApiKey and authToken parameters are required"
  },
  "OTP_VERIFIED_LIMIT_REACHED": {
    code: 429,
    message: "Maximum number of OTP verify requests"
  },
  "CURRENTPASSWORD_AND_NEWPASSWORD_ARE_REQUIRED": {
    code: 400,
    message: "Current password and New Password are Required"
  },
  "PASSWORD_AND_CONFIRM_PASSWORD_ARE_NOT_MATCHING": {
    code: 422,
    message: 'Password and confirm password do not match'
  },
  "OTP_NOT_VERIFIED": {
    code: 401,
    message: "OTP not verified."
  },
  "TOKEN_EXPIRED": {
    code: 401,
    message: "Token expired."
  },
  "TOKEN_REQUIRED": {
    code: 401,
    message: "Token is required."
  },
  "OTP_VERIFY_LIMIT_REACHED": {
    code: 401,
    message: "OTP verify limit reached."
  },
  "TYPE_REQUIRED": {
    code: 400,
    message: "Type is required."
  },
  "PHONE_AND_OTP_REQUIRED": {
    code: 400,
    message: "Phone number and otp are required."
  },
  "USER_ID_IS_REQUIRED": {
    code: 400,
    message: "User id is required."
  },
  // "Wrong OTP entered"
  "INVALID_PRODUCT_ID": {
    code: 400,
    message: "Invalid product id"
  },
  "INVALID_SHARE_ID": {
    code: 400,
    message: "Invalid share id"
  }
};



export default statusCode;

