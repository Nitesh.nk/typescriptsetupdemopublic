var Redis = require("ioredis");
var config = require("../config/config.js");
var redis = new Redis(config.redis);

class RedisHelper {

    ///the function to get the key from the redis
    async redisGetKey(key_name: string, pipeLineObj: any): Promise<string | boolean> {
        try {
            if (pipeLineObj) {
                pipeLineObj.get(key_name);
                return true;
            }
            let value = await redis.get(key_name);
            return value;
        }
        catch (err) {
            throw new Error(err)
        }
    }

///the function to set the key in the redis 
async redisSetKey(key_name: string, value: string, pipeLineObj: any): Promise<string | boolean> {
        try {
            if (pipeLineObj) {
                pipeLineObj.get(key_name);
                return true;
            }
            let valueFromRedis = await redis.set(key_name,value);
            return valueFromRedis;
        }
        catch (err) {
            throw new Error(err)
        }
    }

    ///the function to increment in redis by given value
    async redisIncByAtomic(key_name: string, value: string, pipeLineObj: any): Promise<boolean> {
        try {
            if (pipeLineObj) {
                pipeLineObj.get(key_name, value);
                return true;
            }
            await redis.incrby(key_name, value);
            return true;
        }
        catch (err) {
            throw new Error(err)
        }
    }

    ///the function to inrement by single value
    async redisIncSingle(key_name: string, pipeLineObj: any): Promise<boolean> {
        try {
            if (pipeLineObj) {
                pipeLineObj.get(key_name);
                return true;
            }
            await redis.incr(key_name);
            return true;
        }
        catch (err) {
            throw new Error(err)
        }
    }
}


export default new RedisHelper();
