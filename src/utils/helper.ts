import { logtraces } from "./logger";

export function handleStringParam(value: any): string {
    if(value && typeof value === "string") return value;
    else return "";
}

export function safeParse(data: any) {
    try {
        return JSON.parse(data);
    }
    catch(err) {
        return "";
    }
}

export async function getIdsFromData(data: any=[]): Promise<string[]>  {
    try {
        let ids: string[]=[];
        if(data && data.length) {
            data.forEach((d: any) => {
                if(d.id) ids.push(d.id);
            });
        }
        return ids;
    }
    catch(err) {
        throw err;
    }
}

export async function getUniqueValueCountFromArray(data: string[]): Promise<any> {
    try {
        var count:any = {};
        if(data.length) data.forEach(function(i) { count[i] = (count[i]||0) + 1;});

        return count;
    }
    catch(err) {
        throw err;
    }
}

export async function getUniqueValuesFromArray(data: string[]): Promise<string[]> {
    try {
        let ids: string[]=[];
        for(let d of data) {
            if(!ids.includes(d)) ids.push(d);
        }

        return ids;
    }
    catch(err) {
        throw err;
    }
}

export async function getSelectedDataFromArrayOfObject(data: any[]=[], selectedIds: string[]): Promise<any[]> {
    try {
        let finalData: any[] =[];
        if(data && data.length) {
            for(let d of data) {
                if(selectedIds.includes(d.id)) finalData.push(d);
            }
        }
        return finalData;
    }
    catch(err) {
        throw err;
    }
}

export function sortDataByIds(ids: string[], data: any[]) {
    try {
        let finalPosts: any[] = [], dataObj: any = {};
        data.forEach((d: any) => dataObj[d.id] = d);
        ids.forEach((id: string) => {
            if(dataObj[id]) finalPosts.push(dataObj[id]);
        });
        
        return finalPosts;
    }
    catch(err) {
        logtraces(err, __filename, "sortDataByIds");
        return data;
    }
}