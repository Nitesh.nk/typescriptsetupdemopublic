import express from 'express';

export interface Request extends express.Request {
    originalUrl: string ;
    headers : any;
    token ?: string,
    decoded ?: any,
}

export interface Response extends express.Response {};

export interface NextFunction extends express.NextFunction {};