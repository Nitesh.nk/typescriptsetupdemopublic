'use strict';
const Sequelize = require('sequelize');

class VoucherTypes extends Sequelize.Model{
  static init(sequelize, DataTypes){
    return super.init({
      id: { 
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUID
      },
      is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      }
    },
    { sequelize, freezeTableName: true }
  )
  }

  static associate(models) {
    // associations can be defined here
    this.hasMany(models.TransactionLogs, { foreignKey: 'voucher_type_id', sourceKey: 'id' })
    this.belongsTo(models.Vouchers, {foreignKey: 'voucher_id'})
  }
}

module.exports = VoucherTypes;