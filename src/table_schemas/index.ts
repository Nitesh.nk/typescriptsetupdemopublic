import fs from 'fs';
import path from 'path';
import config from '../config/config';
import { Sequelize, Op } from 'sequelize';

const operatorsAliases: any = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col
};

// Making a connection
const sequelizeCon = new Sequelize(config.db, { operatorsAliases });

const finalRefs: any = {};
// Initialising connections to tables
fs.readdirSync(__dirname)
    .forEach((file: string) => {
        if (!file.endsWith(".js") || file.startsWith('index')) return;
        let model = require(path.resolve(__dirname, file));
        finalRefs[model.name] = model.init(sequelizeCon, Sequelize);
    });

// Making table connection associations
for (let modelName in finalRefs) {
    if (finalRefs[modelName].associate) finalRefs[modelName].associate(finalRefs);
}

export default { sequelizeCon, ...finalRefs };