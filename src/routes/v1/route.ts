// Dependency
import Router from 'express';

import authController from './../../controllers/middlewares/authHandler';
import dataController from "../../controllers/dataController";

const routerV1: Router.Router = Router();

routerV1.get("/get/categories", authController.verifytoken ,dataController.getCategories);

export default routerV1;