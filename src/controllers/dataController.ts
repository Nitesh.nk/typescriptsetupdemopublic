import { logErrorAndSendToErrHandler, successHandler } from "../utils/responseHandlers";
import * as ei from "./../typescript_helpers/interfaces/express";
import statusCode from "../utils/statusCode";
import Vouchers from '../table_schemas/voucher';
import VoucherTypes from '../table_schemas/vouchertype'
import redisHelper from '../utils/redisHelper'
// import ProductInterface from "../typescript_helpers/interfaces/productInterface";

class DataController {

    async getCategories(req: ei.Request, res: ei.Response): Promise<void> {
        try {
            let coucherListInDB: any=[];
            let test:string | boolean=await redisHelper.redisGetKey("Nitesh",null);
            console.log("Nitesh ",test)
            coucherListInDB = await Vouchers.findAll({
                include: [{
                    model: VoucherTypes,
                }]
            });
            return successHandler(statusCode.SUCCESS.message, coucherListInDB, req, res);
        }
        catch(err) {
            return logErrorAndSendToErrHandler(req, statusCode.SOMETHING_WENT_WRONG.code, statusCode.SOMETHING_WENT_WRONG.message, res, "getCategories", req.originalUrl, err);
        }
    }

}

export default new DataController();