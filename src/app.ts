import path from 'path';
require('dotenv').config({ path: path.resolve("./.env") });

'use-strict'
// Application insights settings
declare global {
    namespace NodeJS {
        interface Global {
            applocationInsightsClientRef: any;
        }
    }
}
global.applocationInsightsClientRef = require('./utils/applicationInsights');

import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import morgan from 'morgan';

import v1 from "./routes/v1/route";
import config from "./config/config";
import * as ei from "./typescript_helpers/interfaces/express";

require('./table_schemas');

//Initiate app
const app = express();

//Middlewares
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(helmet());
app.use(bodyParser.json());
app.use(morgan('combined'));


app.use('/api/v1/', v1);

// error handler middleware
interface Error {
    status?: number;
    message?: string;
}

app.use(function (_req, res, next) {
    const err = new Error('Resource not found');
    res.status(404)
    next(err);
});

app.use((err: Error, _req: ei.Request, res: ei.Response, _next: ei.NextFunction) => {
    res.status(err.status || 404).send({
        "success": false,
        "message": err.message || 'Resource not found',
        "code": err.status || 404,

    });
});


app.listen(config.port, () => {
    console.log(`Server started on port ${config.port}`);
});